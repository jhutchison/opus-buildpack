#Heroku buildpack: opus

This is a Heroku buildpack for using lib opus in your project.

# Usage

For a new project:

``` sh 
$ heroku create --buildpack https://ingmferrer@bitbucket.org/ingmferrer/opus-buildpack.git
```

If it is an existing project, just do:

``` sh 
$ heroku buildpacks:set https://ingmferrer@bitbucket.org/ingmferrer/opus-buildpack.git
```